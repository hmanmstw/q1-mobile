import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'login/0', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  }, 
  {
    path: 'login/:type',
    loadChildren: () => import('./pages/item-details-login/item-details-login.module').then(m => m.ItemDetailsLoginPageModule)
  },
  {
    path: 'register/:type',
    loadChildren: () => import('./pages/item-details-register/item-details-register.module').then(m => m.ItemDetailsRegisterPageModule)
  },
  {
    path: 'forgot-password/:type',
    loadChildren: () => import('./pages/item-details-forgot-password/item-details-forgot-password.module').then(m => m.ItemDetailsForgotPasswordPageModule)
  },
  {
    path: 'new-password/:type',
    loadChildren: () => import('./pages/item-details-new-password/item-details-new-password.module').then(m => m.ItemDetailsNewPasswordPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/item-details-settings/item-details-settings.module').then(m => m.ItemDetailsSettingsPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,  { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
