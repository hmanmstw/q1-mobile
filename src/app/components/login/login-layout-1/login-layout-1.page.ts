import { Component, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'cs-login-layout-1',
  templateUrl: 'login-layout-1.page.html',
  styleUrls: ['login-layout-1.page.scss'],
})
export class LoginLayout1Page implements OnChanges {
  @Input() data: any;

  @Output() onLogin = new EventEmitter();

  private isUsernameValid = true;
  private isPasswordValid = true;

  item = {
    'username': '',
    'password': ''
  };

  constructor(private navController: NavController, private router: Router) { 

  }
  
  ngOnChanges(changes: { [propKey: string]: any }) {
    this.data = changes['data'].currentValue;
  }

  onLoginFunc(): void {
    if (event) {
      event.stopPropagation();
    }
    if (this.validate()) {
      this.onLogin.emit(this.item);
    }
  }
 
  validate(): boolean {
    this.isUsernameValid = true;
    this.isPasswordValid = true;
    if (!this.item.username || this.item.username.length === 0) {
      this.isUsernameValid = false;
    }

    if (!this.item.password || this.item.password.length === 0) {
      this.isPasswordValid = false;
    }

    return this.isPasswordValid && this.isUsernameValid;
  }

  goToSignup() {
    this.router.navigate(['/register/0']);
  }
}
