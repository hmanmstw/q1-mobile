import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { FilterPipe } from './search-bar/FilterPipe';
import { ParallaxHeader } from './parallax/parallax-header';
import { AgmCoreModule } from '@agm/core';

import { LoginLayout1Page } from './login/login-layout-1/login-layout-1.page';
import { RegisterLayout1Page } from './register/register-layout-1/register-layout-1.page';
import { ForgotPasswordLayout1Page } from './forgot-password/forgot-password-layout-1/forgot-password-layout-1.page';
import { NewPasswordLayout1Page } from './new-password/new-password-layout-1/new-password-layout-1.page';
import { FormsLayout3Page } from './forms/forms-layout-3/forms-layout-3.page';
import { AnimateItemsDirective } from '../directives/animate-items.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgmCoreModule.forRoot({ apiKey: '' })
  ],
  declarations: [
    AnimateItemsDirective,
    FilterPipe, ParallaxHeader, LoginLayout1Page,
    RegisterLayout1Page, 
    ForgotPasswordLayout1Page,
    NewPasswordLayout1Page,
    FormsLayout3Page
  ],
  exports: [
    FilterPipe, ParallaxHeader, LoginLayout1Page, 
    RegisterLayout1Page, 
    ForgotPasswordLayout1Page, 
    NewPasswordLayout1Page,
    FormsLayout3Page
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
