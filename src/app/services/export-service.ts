import { HomeService } from './home-service';
import { IntroService } from './intro-service';
import { TabsService } from './tabs-service';
import { MenuService } from './menu-service';
import { NewPasswordService } from './new-password-services';
import { ForgetPasswordService } from './forget-password-services';
import { AlertService } from './alert-service';
import { CheckBoxService } from './check-box-service';
import { SearchBarService } from './search-bar-service';
import { WizardService } from './wizard-service';
import { LoginService } from './login-service';
import { RegisterService } from './register-service';
import { ParallaxService } from './parallax-service';
import { MapsService } from './maps-service';
import { RadioButtonService } from './radio-button-service';
import { ToggleService } from './toggle-service';
import { SelectService } from './select-service';
import { ActionSheetService } from './action-sheet-service'
import { FormService } from './form-service';
import { ProfileService } from './profile-service';
import { Injectable } from '@angular/core';

@Injectable()
export class ExportService{
    export() {
        return {           
            //'actionSheet': this.getActionSheet(),
            //'alert': this.getAlert(),
            //'checkBoxes': this.getCheckBox(),
            'forgetPassword': this.getForgetPassword(),
            'form': this.getForm(),
            'menu':this.getMenu(),
            'home': this.getHome(),
            //'intro': this.getIntro(),
            'login': this.getLogin(),
            //'maps':this.getMaps(),
            'newPassword' : this.getNewPassword(),
            //'parallax':this.getParallax(),
            //'profile':this.getProfile(),
            //'radioButton': this.getRadioButton(),
            'register': this.getRegister(),
            //'searchBars':this.getSearchBar(),
            //'select':this.getSelect(),
            //'tab':this.getTabs(),
            //'toggle':this.getToggle(),
            //'wizard':this.getWizard()
        }
    }

    getNewPassword() {
        let service = new NewPasswordService(null, null);
        return {
            'layout1': service.getDataForLayout1()
        }
    }

    getForgetPassword() {
        let service = new ForgetPasswordService(null, null);
        return {
            'layout1': service.getDataForLayout1()
        }
    }

    getLogin() {
        let service = new LoginService(null, null);
        return {
            'layout1': service.getDataForLayout1()
        }
    }

    getRegister() {
        let service = new RegisterService(null, null);
        return {
            'layout1': service.getDataForLayout1()
        }
    }

    getForm() {
        let service = new FormService(null, null, null);
        return {
            'layout3': service.getDataForLayout3()
        }
    }
    
    getMenu() {
        let service = new MenuService(null, null);
        return service.getDataForTheme(null)
    }
    
    getIntro() {
        let service = new IntroService(null, null);
        return service.getData()
    }

    getHome() {
        let service = new HomeService(null);
        return service.getData()
    }

}