import { IService } from './IService';
import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
import { LoadingService } from './loading-service';

@Injectable({ providedIn: 'root' })
export class MenuService implements IService {

  constructor(public af: AngularFireDatabase, private loadingService: LoadingService) { }

  getId = (): string => 'menu';

  getTitle = (): string => 'Q1Mobile';

  //* Data Set for main menu
  getAllThemes = (): Array<any> => {
    return [      
      {
        'url': 'home',
        'title': 'Home',
        'theme': 'login',
        'icon': 'icon-home',
        'listView': false,
        'component': '',
        'singlePage': true
      },
      {
        'url': 'login/0',
        'title': 'Login',
        'theme': 'login',
        'icon': 'icon-lock-open-outline',
        'listView': false,
        'component': '',
        'singlePage': true
      },
      {
        'url': 'register/0',
        'title': 'Signup',
        'theme': 'register',
        'icon': 'icon-comment-account',
        'listView': false,
        'component': '',
        'singlePage': true
      },
      {
        'url': 'forgot-password/0',
        'title': 'Forgot Password',
        'theme': 'forgetPassword',
        'icon': 'icon-eye-off',
        'listView': false,
        'component': '',
        'singlePage': true
      },
      {
        'url': 'new-password/0',
        'title': 'Change Password',
        'theme': 'newPassword',
        'icon': 'icon-key-variant',
        'listView': false,
        'component': '',
        'singlePage': true
      },
      {
        'url': 'settings',
        'title': 'Admin',
        'theme': 'demo',
        'icon': 'icon-settings',
        'listView': false,
        'component': '',
        'singlePage': true
      }
    ];
  }

  getDataForTheme = (menuItem: any) => {
    return {
      'background': 'assets/imgs/background-small/0.jpg',
      'image': 'assets/imgs/logo/5.png',
      'title': 'Q1 Mobile'
    };
  }

  getEventsForTheme = (menuItem: any): any => {
    return {};
  }

  prepareParams = (item: any) => {
    return {
      title: item.title,
      data: {},
      events: this.getEventsForTheme(item)
    };
  }

  load(item: any): Observable<any> {
    this.loadingService.show();
    if (AppSettings.IS_FIREBASE_ENABLED) {
      return new Observable(observer => {
        this.af
          .object('menu')
          .valueChanges()
          .subscribe(snapshot => {
            this.loadingService.hide();
            observer.next(snapshot);
            observer.complete();
          }, err => {
            this.loadingService.hide();
            observer.error([]);
            observer.complete();
          });
      });
    } else {
      return new Observable(observer => {
        this.loadingService.hide();
        observer.next(this.getDataForTheme(item));
        observer.complete();
      });
    }
  }
}
